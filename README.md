# Overall flow of jsons in Project 3

![Flowchart](https://i.imgur.com/cw1Oc9I.png)

# Assessment Ingestion

## **Input**

```json
{
	"email": "BillyMcAssociate@revature.net",
	"exerciseId": 201,
	"tests": [
		{ "testName": "create_user_account", "isSuccessful": true, "points": 10, "errorMessage": "SUCCESS" },
		{ "testName": "update_user_account", "isSuccessful": false, "points": 20, "errorMessage": "error at line 45" },
		{ "testName": "delete_user_account", "isSuccessful": false, "points": 15, "errorMessage": "error at line 876" }
	],
	"sourceCode": "afwefnpim23r082jq0if0in2080vq28nf02830q234q09jfosnfoiwnf2o3n203f8nq023f8nq20398",
	"assessmentTime": 1633613342 // epoch time
}
```

## **Output**

-   Request to RevAssess REST API to validate exerciseId 201 (/exercises/201), if results returned
    -   Log Entry
    -   Submit to message of JSON Pub/Sub

```json
{
	"email": "BillyMcAssociate@revature.net",
	"exerciseId": 201,
	"tests": [
		{ "testName": "create_user_account", "isSuccessful": true, "points": 10, "errorMessage": "SUCCESS" },
		{ "testName": "update_user_account", "isSuccessful": false, "points": 20, "errorMessage": "error at line 45" },
		{ "testName": "delete_user_account", "isSuccessful": false, "points": 15, "errorMessage": "error at line 876" }
	],
	"sourceCode": "afwefnpim23r082jq0if0in2080vq28nf02830q234q09jfosnfoiwnf2o3n203f8nq023f8nq20398",
	"assessmentTime": 1633613342 // epoch time
}
```

# Pub/Sub

## **Input**

```json
{
	"email": "BillyMcAssociate@revature.net",
	"exerciseId": 201,
	"tests": [
		{ "testName": "create_user_account", "isSuccessful": true, "points": 10, "errorMessage": "SUCCESS" },
		{ "testName": "update_user_account", "isSuccessful": false, "points": 20, "errorMessage": "error at line 45" },
		{ "testName": "delete_user_account", "isSuccessful": false, "points": 15, "errorMessage": "error at line 876" }
	],
	"sourceCode": "afwefnpim23r082jq0if0in2080vq28nf02830q234q09jfosnfoiwnf2o3n203f8nq023f8nq20398",
	"assessmentTime": 1633613342 // epoch time
}
```

## **Output**

```json
{
	"email": "BillyMcAssociate@revature.net",
	"exerciseId": 201,
	"tests": [
		{ "testName": "create_user_account", "isSuccessful": true, "points": 10, "errorMessage": "SUCCESS" },
		{ "testName": "update_user_account", "isSuccessful": false, "points": 20, "errorMessage": "error at line 45" },
		{ "testName": "delete_user_account", "isSuccessful": false, "points": 15, "errorMessage": "error at line 876" }
	],
	"sourceCode": "afwefnpim23r082jq0if0in2080vq28nf02830q234q09jfosnfoiwnf2o3n203f8nq023f8nq20398",
	"assessmentTime": 1633613342 // epoch time
}
```

# Assessment Uploader

## **Input**

```json
{
	"email": "BillyMcAssociate@revature.net",
	"exerciseId": 201,
	"tests": [
		{ "testName": "create_user_account", "isSuccessful": true, "points": 10, "errorMessage": "SUCCESS" },
		{ "testName": "update_user_account", "isSuccessful": false, "points": 20, "errorMessage": "error at line 45" },
		{ "testName": "delete_user_account", "isSuccessful": false, "points": 15, "errorMessage": "error at line 876" }
	],
	"sourceCode": "afwefnpim23r082jq0if0in2080vq28nf02830q234q09jfosnfoiwnf2o3n203f8nq023f8nq20398",
	"assessmentTime": 1633613342 // epoch time
}
```

## **Output**

    -   To Code Bucket

```json
{
	"sourceCode": "afwefnpim23r082jq0if0in2080vq28nf02830q234q09jfosnfoiwnf2o3n203f8nq023f8nq20398"
}
```

-   To RevAccess DB
    -   Assessment Creation to **SQL**

```json
{
	"email": "BillyMcAssociate@revature.net",
	"exercise_id": 201,
	"source_link": "bucket_url_here",
	"assessment_time": 1633613342 // epoch time
}
```

-   To RevAccess DB
    -   Test Creation to **SQL**

```json
[
	{ "test_name": "create_user_account", "is_successful": true, "points": 10, "error_message": "SUCCESS" },
	{ "test_name": "update_user_account", "is_successful": false, "points": 20, "error_message": "error at line 45" },
	{ "test_name": "delete_user_account", "is_successful": false, "points": 15, "error_message": "error at line 876" }
]
```

# Code Bucket

## **Input**

```json
{
	"sourceCode": "afwefnpim23r082jq0if0in2080vq28nf02830q234q09jfosnfoiwnf2o3n203f8nq023f8nq20398"
}
```

## **Output**

```json
{
	"sourceLink": "bucket_url_here"
}
```

# Rev Access DB

## **Inputs**

-   Assessment Uploader
    -   Assessment Creation

```json
{
	"email": "BillyMcAssociate@revature.net",
	"exercise_id": 201,
	"source_link": "bucket_url_here",
	"assessment_time": 1633613342 // epoch time
}
```

-   Assessment Uploader
    -   Test Creation

```json
[
	{ "test_name": "create_user_account", "is_successful": true, "points": 10, "error_message": "SUCCESS" },
	{ "test_name": "update_user_account", "is_successful": false, "points": 20, "error_message": "error at line 45" },
	{ "test_name": "delete_user_account", "is_successful": false, "points": 15, "error_message": "error at line 876" }
]
```

-   REST API
    -   Exercise Creation

```json
// JSON input Expected
{
	"exercise_id": 0,
	"exercise_name": "RevExampleUpload",
	"repo_link": "https://gitlab.com/RevMaster/RevExampleUpload",
	"max_points": 150
}
```

# REST API

## SQL DB Schema

![RevAssessSchema](https://i.imgur.com/xotGk8n.png)

## Features

-   After receiving a JWT
-   Authenticate Authorization token

## Input

```json
{
	"header": {
		"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6IlRyYWluZXJAcmV2YXR1cmUubmV0IiwiaWF0IjoxNTE2MjM5MDIyfQ.U8ta-Q6Z7U7X-EWmBb8HsCCJMvV81-zKOH5IgmqkF7c"
	}
}
```

### POSTS

-   /exercises
    -   Creates an exercise to the DB

```json
// JSON input Expected
{
	"exerciseId": 0,
	"exerciseName": "RevExampleUpload",
	"repoLink": "https://gitlab.com/RevMaster/RevExampleUpload",
	"maxPoints": 150
}
```

### GETs

-   /exercises
    -   An array of all exercises
-   /exercises/:id
    -   A singular exercise

```json
// JSON output for above routes (:id will provide an individual objects)
[
	{
		"exerciseId": 1,
		"exerciseName": "RevExampleExercise",
		"repoLink": "https://gitlab.com/RevMaster/RevTestExample",
		"maxPoints": 100
	},
	{
		"exerciseId": 2,
		"exerciseName": "RevExampleExercise2",
		"repoLink": "https://gitlab.com/RevMaster/RevTestExample2",
		"maxPoints": 120
	}
]
```

-   /exercises/:id/assessments
    -   All assessments linked to an exercise
-   /exercises/:id/assessments/:email
    -   An individual Associate's assessments linked to an exercise, filtered by their email
-   /exercises/:id/assess/recent
    -   An array of each Associates most recent assessment score
-   /exercises/:id/assess/max
    -   An array of each Associates maximum assessment score
-   /assessments
    -   An array of all assessments
-   /assessments/:id
    -   Individual Assessment

```json
// JSON output for above routes
[
	{
		"assessmentId": 1,
		"email": "BillyMcAssociate@mail.com",
		"assessmentTime": 1633613342, // get an actual epoch number
		"exerciseId": 1,
		"sourceLink": "code_bucket_url_here",
		"score": 50 // Calculated from test table in SQL
	},
	{
		"assessmentId": 2,
		"email": "BillyMcAssociate@mail.com",
		"assessmentTime": 1633613342, // get an actual epoch number
		"exerciseId": 1,
		"sourceLink": "code_bucket_url_here",
		"score": 100 // Calculated from test table in SQL
	}
]
```

-   /assessments/:id/tests
    -   information on each test performed on the assessment

```json
// JSON output for above routes
[
	{
		"testId": 1,
		"testName": "getIdRequestTest",
		"isSuccessful": true,
		"points": 50,
		"errorMessage": "SUCCESS",
		"assessmentId": 1
	},
	{
		"testId": 2,
		"testName": "getAllTest",
		"isSuccessful": false,
		"points": 50,
		"errorMessage": "error at line 800",
		"assessmentId": 1
	}
]
```

### DELETE

-   These only require an ID number specified in the params to be deleted
-   They will simply return a true status
    -   /exercises/:id

# Authorization

## **Input**

```json
{
	"email": "Trainer@revature.com",
	"password": "totallyAHashedPassword2!"
}
```

## **Output**

-   JWT to be stored in local storage on frontend

```json
{
	"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6IlRyYWluZXJAcmV2YXR1cmUubmV0IiwiaWF0IjoxNTE2MjM5MDIyfQ.U8ta-Q6Z7U7X-EWmBb8HsCCJMvV81-zKOH5IgmqkF7c"
}
```

# Frontend

-   MaterialUI
-   Inputs from all services requiring frontend
